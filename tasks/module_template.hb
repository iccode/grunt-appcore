define([
'appcore', 'Handlebars', {{{requirements}}}, 'Handlebars'
], function(appcore, Handlebars, {{{defargs}}}) {

    var module = new appcore.Module('{{name}}', {
        'routes': {
            {{{routes}}}
        }
    });

    module.__TEMPLATES = {};
    {{#each templates}}
    module.__TEMPLATES['{{this.0.}}'] = Handlebars.template({{{this.1.}}});
    {{/each}}

    module.template = function(tmpl, args){
        return module.__TEMPLATES[tmpl](args || window);
    };

    {{#each exportedFunctions}}
    module['{{this.0.}}'] = {{{this.1.}}};
    {{/each}}
    
    {{#each defargs}}
    module['{{this}}'] = {{this}};
    {{/each}}


    return module;
});
