/*
 * grunt-appcore
 * 
 *
 * Copyright (c) 2013 Tasos Vogiatzoglou
 * Licensed under the GPL license.
 */

'use strict';

module.exports = function(grunt) {

  var hb = require('handlebars'),
      rjs = require('requirejs'),
      vm = require('vm'),
      path = require('path'),
      fs = require('fs'),
      MOD_DIR = path.dirname(module.filename),
      MODULE_TEMPLATE = hb.compile(fs.readFileSync(path.join(MOD_DIR,'module_template.hb')).toString()),
      PARTIALS_TEMPLATE = hb.compile(fs.readFileSync(path.join(MOD_DIR,'partials_template.hb')).toString()),
      CONTEXT = {
          'defineModule': function(name, args){
              return [name, args];
            }
      }

  grunt.registerMultiTask('partials', 'Combine partials to a amd module', function() {
    var options = this.options({
            'dir':'',
            'target': ''
        }), 
        partials = [];

    fs.readdirSync(path.resolve(options.dir)).each(function(e) {
        var data = fs.readFileSync(path.join(path.resolve(options.dir), e)).toString();
        partials.push([path.basename(e, path.extname(e)), hb.precompile(data).toString()]);
    });

    grunt.file.write(options.target, PARTIALS_TEMPLATE({partials:partials}));
      
  });

  grunt.registerMultiTask('appcore', 'Your task description goes here.', function() {
    
    var options = this.options({
            'base': '.',
            'rconfig': null }), 
        base = options.base, 
        appDir = path.join(options.base, 'app'),
        moduleDir = path.join(appDir, 'modules');
      
    grunt.log.writeln('Running appcore build');
    grunt.log.writeln(JSON.stringify(options));

    function compressModule(module) {
        grunt.log.writeln('Compressing '+module+' ...');
        var templates = compileTemplates(module),
            moduleDefinition = getModuleDefinition(module),
            module_script = null;
            
        moduleDefinition.templates = templates;
        moduleDefinition.routes = moduleDefinition.routes.join("\n,");

        module_script = MODULE_TEMPLATE(moduleDefinition);
        
        grunt.file.write(path.join(moduleDir, module+".js"), module_script);
    }

    function makeRoutes(routes_dict) {
        grunt.log.writeln('\tMaking routes');
        var routes = [], route_names = Object.keys(routes_dict);


        return routes;
    }

    function fixPath(module, r) {
        var modPath = path.join(moduleDir, module+".js"),
            rPath = path.resolve(path.join(moduleDir, module, r)),
            relPath = path.relative(path.dirname(modPath), rPath);

        return '"./'+relPath+'"';
    }

    function getModuleDefinition(module) {
        grunt.log.writeln('\tGetting module definition');
        var moduleScriptName = path.join(moduleDir, module, 'module.js'),
            moduleScript = fs.readFileSync(moduleScriptName).toString(),
            res = vm.runInNewContext(moduleScript, CONTEXT),
            export_keys = Object.keys(res[1].exports),
            route_names = Object.keys(res[1].routes),
            moduleDefinition = {
                'name': res[0],
                'requirements': [],
                'defargs' : [],
                'exportedFunctions': [],
                'routes' : []
            }, i;

        for (i=0; i<export_keys.length; i++){
            var exp = res[1].exports[export_keys[i]];
            if (typeof exp === "string") {
                moduleDefinition.requirements.push(fixPath(module, exp));
                moduleDefinition.defargs.push(export_keys[i]);
            } else if (typeof exp === "function") {
                moduleDefinition.exportedFunctions.push([export_keys[i],exp.toString()]);
            }
        }

        for (var i=0;i<route_names.length;i++){
            moduleDefinition['routes'].push("'"+route_names[i]+"' :"+ res[1].routes[route_names[i]].toString());
        }

        return moduleDefinition;
    }

    function compileTemplates(module) {
        grunt.log.writeln("\tCompiling templates");
        var templateDir = path.join(moduleDir, module, 'templates'), 
            templates = [],
            compiledTemplates = [];

        try {
            templates = fs.readdirSync(templateDir);
        }catch (e){
        }

        compiledTemplates = templates.map(function(templateName){
            var f = path.join(templateDir, templateName),
                data = fs.readFileSync(f).toString();
            return [templateName, hb.precompile(data)]
        });

        return compiledTemplates;
    }

    fs.readdirSync(moduleDir).each(function(f) {
        if (fs.statSync(path.join(moduleDir, f)).isDirectory()) {
            compressModule(f);
        }
    });








  });

};
