define(['Handlebars'], function(Handlebars) {
    {{#each partials}}
    Handlebars.registerPartial('{{this.0.}}',Handlebars.template({{{this.1.}}}));
    {{/each}}
});
